import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Marche } from '../models/marche.model';
import { MarcheService } from '../services/marche.service';

@Component({
  selector: 'app-edit-market',
  templateUrl: './edit-market.component.html',
  styleUrls: ['./edit-market.component.css']
})
export class EditMarketComponent implements OnInit {
  marche: Marche = new Marche();
  id: number;
  constructor(private marcheService: MarcheService, private route: Router, private activateRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.params['id'];
    //console.log(this.id);
    this.marcheService.getMarchesById(this.id)
      .subscribe(data => {
        this.marche = data;
        //console.log(this.marche);
      }, error => console.log(error));
  }

  gotoMarcheList() {
    this.route.navigate(['/market']);
  }


  onSubmit() {
    this.marcheService.updateMarche(this.id, this.marche).subscribe(data => {
      alert("Modification effectuée avec succès");
      this.gotoMarcheList();
    }, error => console.log(error));
  }


}
