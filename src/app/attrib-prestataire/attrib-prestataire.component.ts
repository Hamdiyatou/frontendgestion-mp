import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Marcheprestataire } from '../models/marche-prestataire';
import { Marche } from '../models/marche.model';
import { Prestataire } from '../models/prestataire.model';
import { MarchePrestataireService } from '../services/marche-prestataire.service';
import { MarcheService } from '../services/marche.service';
import { PrestataireService } from '../services/prestataire.service';

@Component({
  selector: 'app-attrib-prestataire',
  templateUrl: './attrib-prestataire.component.html',
  styleUrls: ['./attrib-prestataire.component.css']
})
export class AttribPrestataireComponent implements OnInit {

  public prestataires: Prestataire[];
  public prestataireId: number;

  public marcheId: number;
  public marches: Marche[];

  public marchePrest: Marcheprestataire = new Marcheprestataire();
  public marchePrests: Marcheprestataire[];

  constructor(private marchePrestService: MarchePrestataireService, private router: Router, private prestataireService: PrestataireService, private marketService: MarcheService, private activatedRoute: ActivatedRoute, private marcheService: MarcheService) { }

  ngOnInit(): void {

    this.prestataireService.getPrestataires().subscribe(data => {
      if (data) {
        this.prestataires = data;
        // console.log(this.prestataires);
      }
    });

    this.marketService.getMarches().subscribe(data => {
      if (data) {
        this.marches = data;
        // console.log(this.marches);
      }
    });

  }

  save(marchePrest: Marcheprestataire) {
    // console.log(this.marchePrest);
    marchePrest.marche = this.marches.find((next) => next.id == this.marcheId);

    marchePrest.prestataire = this.prestataires.find((next) => next.id == this.prestataireId);
       console.log(JSON.stringify(marchePrest));
    this.marchePrestService.saveMarchePrestataire(this.marchePrestService.host+"/saveMarcheprestataire", marchePrest)
      .subscribe(res => {
        console.log(res);
        this.marchePrest = res;
        alert("Enregistrement réussi");
        this.router.navigateByUrl("/list-marchePrestataire");
      }, err => {
        console.log(err);
        alert("Echec de l'enregistrement");
      });
  }


}
