import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttribPrestataireComponent } from './attrib-prestataire.component';

describe('AttribPrestataireComponent', () => {
  let component: AttribPrestataireComponent;
  let fixture: ComponentFixture<AttribPrestataireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttribPrestataireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttribPrestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
