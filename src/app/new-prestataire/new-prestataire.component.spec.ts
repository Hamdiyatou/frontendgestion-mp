import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPrestataireComponent } from './new-prestataire.component';

describe('NewPrestataireComponent', () => {
  let component: NewPrestataireComponent;
  let fixture: ComponentFixture<NewPrestataireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewPrestataireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPrestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
