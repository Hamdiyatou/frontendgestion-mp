import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Category } from '../models/categorie.model';
import { Prestataire } from '../models/prestataire.model';
import { CategorieService } from '../services/categorie.service';
import { PrestataireService } from '../services/prestataire.service';

@Component({
  selector: 'app-new-prestataire',
  templateUrl: './new-prestataire.component.html',
  styleUrls: ['./new-prestataire.component.css']
})
export class NewPrestataireComponent implements OnInit {

  prestataire:Prestataire = new Prestataire();
  categories:Category[];
  categorieId?: number;

  constructor(private prestataireService:PrestataireService, private categorieService:CategorieService,private router:Router) { }

  ngOnInit(): void {

    this.categorieService.getCategories()
      .subscribe(data => {
        if (data) {
          this.categories = data;
        }
      });

  }

  save(prestataire: Prestataire) {
    // console.log(this.categorieId);
    prestataire.category = this.categories.find((next) => next.id == this.categorieId);
    // console.log(JSON.stringify(this.categories));
    // console.log(JSON.stringify(this.categories.find((next) => next.id == this.categorieId)));
    // console.log(JSON.stringify(prestataire));
    this.prestataireService.savePrestataire(this.prestataireService.host+"/savePrestataire", prestataire)
    .subscribe(res => {
      prestataire=res;
      alert("Enregistrement réussi");
      this.router.navigateByUrl("/prestataire");
    }, err=>{
      alert("Echec de l'enregistrement");
    });
  }
}
