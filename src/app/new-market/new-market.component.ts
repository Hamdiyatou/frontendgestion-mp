import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Marche } from '../models/marche.model';
import { MarcheService } from '../services/marche.service';

@Component({
  selector: 'app-new-market',
  templateUrl: './new-market.component.html',
  styleUrls: ['./new-market.component.css']
})
export class NewMarketComponent implements OnInit {


  marche: Marche = new Marche();

  constructor(private marcheService: MarcheService,private router:Router) { }

  ngOnInit(): void {
  }

  save(marche: Marche) {
    console.log(this.marche);
    this.marcheService.saveMarche(this.marcheService.host+"/saveMarche", marche)
    .subscribe(res => {
      console.log(res);
      this.marche=res;
      alert("Enregistrement réussi");
      this.router.navigateByUrl("/market");
    }, err=>{
      console.log(err);
      alert("Echec de l'enregistrement");
    });
  }

}
