import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from '../models/categorie.model';
import { Prestataire } from '../models/prestataire.model';
import { CategorieService } from '../services/categorie.service';
import { PrestataireService } from '../services/prestataire.service';

@Component({
  selector: 'app-edit-prestataire',
  templateUrl: './edit-prestataire.component.html',
  styleUrls: ['./edit-prestataire.component.css']
})
export class EditPrestataireComponent implements OnInit {
  id: number;
  prestataire: Prestataire = new Prestataire();

  categories: Category[];
  categorieId?: number;

  constructor(private prestataireService: PrestataireService, private route: Router, private activateRoute: ActivatedRoute, private categorieService: CategorieService) { }

  ngOnInit(): void {

    this.id = this.activateRoute.snapshot.params['id'];
    //console.log(this.id);
    this.prestataireService.getPrestataireById(this.id)
      .subscribe(data => {
        this.prestataire = data;
        //console.log(this.marche);
      }, error => console.log(error));

    this.categorieService.getCategories()
      .subscribe(data => {
        if (data) {
          this.categories = data;
        }
      });

  }


  gotoPrestataireList() {
    this.route.navigate(['/prestataire']);
  }


  onSubmit() {
    this.prestataire.category = this.categories.find((next) => next.id == this.categorieId);
    this.prestataireService.updatePrestataire(this.id, this.prestataire).subscribe(data => {
      alert("Modification effectuée avec succès");
      this.gotoPrestataireList();
    }, error => console.log(error));
  }

}
