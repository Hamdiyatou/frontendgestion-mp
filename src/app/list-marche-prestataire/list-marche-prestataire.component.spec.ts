import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMarchePrestataireComponent } from './list-marche-prestataire.component';

describe('ListMarchePrestataireComponent', () => {
  let component: ListMarchePrestataireComponent;
  let fixture: ComponentFixture<ListMarchePrestataireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListMarchePrestataireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMarchePrestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
