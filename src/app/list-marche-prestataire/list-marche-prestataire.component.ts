import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Marcheprestataire } from '../models/marche-prestataire';
import { Marche } from '../models/marche.model';
import { Prestataire } from '../models/prestataire.model';
import { MarchePrestataireService } from '../services/marche-prestataire.service';
import { MarcheService } from '../services/marche.service';
import { PrestataireService } from '../services/prestataire.service';

@Component({
  selector: 'app-list-marche-prestataire',
  templateUrl: './list-marche-prestataire.component.html',
  styleUrls: ['./list-marche-prestataire.component.css']
})
export class ListMarchePrestataireComponent implements OnInit {

  public prestataires: Prestataire[];
  public prestataireId: number;

  public marcheId: number;
  public marches: Marche[];

  public marchePrest: Marcheprestataire = new Marcheprestataire();
  public marchePrests: Marcheprestataire[];


  constructor(private marchePrestService: MarchePrestataireService, private router: Router) { }

  ngOnInit(): void {

    this.listMarchePrestataire();

  }

  private listMarchePrestataire() {
    this.marchePrestService.getMarchePrestataires().subscribe(data => {
      if (data) {
        this.marchePrests = data;
      } else {
        alert("Echec de chargement");
      }
    });
  }


  delete(id: number) {
    let conf = confirm("Voulez-vous supprimer ?")
    if (conf) {
      this.marchePrestService.deleteMarchePrestataire(id).subscribe(data => {
        console.log(data);
        alert("Suppression effectuée avec succès");
        this.listMarchePrestataire();
      });

    } else {
      alert("Echec lors de la suppression");
    }
  }

  update(id: number) {
    console.log(id);
    this.router.navigate(['edit-marcheprestataire', id]);
  }

}
