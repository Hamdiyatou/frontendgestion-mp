import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { PrestatairesComponent } from './prestataires/prestataires.component';
import { MarketsComponent } from './markets/markets.component';
import { HttpClientModule } from '@angular/common/http';
import { NewMarketComponent } from './new-market/new-market.component';
import { NewPrestataireComponent } from './new-prestataire/new-prestataire.component';
import { FormsModule } from '@angular/forms';
import { AttribPrestataireComponent } from './attrib-prestataire/attrib-prestataire.component';
import { EditMarketComponent } from './edit-market/edit-market.component';
import { EditPrestataireComponent } from './edit-prestataire/edit-prestataire.component';
import { ListMarchePrestataireComponent } from './list-marche-prestataire/list-marche-prestataire.component';
import { EditMarcheprestataireComponent } from './edit-marcheprestataire/edit-marcheprestataire.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    PrestatairesComponent,
    MarketsComponent,
    NewMarketComponent,
    NewPrestataireComponent,
    AttribPrestataireComponent,
    EditMarketComponent,
    EditPrestataireComponent,
    ListMarchePrestataireComponent,
    EditMarcheprestataireComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
