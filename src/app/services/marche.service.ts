import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Marche } from '../models/marche.model';
import { catchError } from 'rxjs/operators';
import { LogService } from './log.service';


@Injectable({
  providedIn: 'root'
})
export class MarcheService {

  public host:string="http://localhost:8080";

  constructor(private httpClient: HttpClient,private logService: LogService) { }

  public getMarches():Observable<Marche[]> {

    return this.httpClient.get<Marche[]>(this.host + "/listMarche")
    .pipe(catchError((error: any): Observable<Marche[]> => {
      this.logService.error(error);
      return of(null);
    }));
 }

 public getMarchesById(id:number):Observable<Marche> {

  return this.httpClient.get<Marche>(`${this.host}/listMarche/${id}`);

}

 public saveMarche(url:any,marche:Marche):Observable<Marche>{

  return this.httpClient.post<Marche>(url, marche);


}

public deleteMarche(id:number):Observable<Marche>{
  return this.httpClient.delete(`${this.host}/deleteMarche/${id}`);
}


public updateMarche(id: number, marche:Marche):Observable<Marche> {

  return this.httpClient.put(`${this.host}/updateMarche/${id}`, marche);
}

}
