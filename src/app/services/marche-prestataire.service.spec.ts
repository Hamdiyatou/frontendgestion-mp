import { TestBed } from '@angular/core/testing';

import { MarchePrestataireService } from './marche-prestataire.service';

describe('MarchePrestataireService', () => {
  let service: MarchePrestataireService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MarchePrestataireService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
