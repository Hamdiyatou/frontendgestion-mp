import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Marcheprestataire } from '../models/marche-prestataire';
import { LogService } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class MarchePrestataireService {

  public host: string = "http://localhost:8080";

  constructor(private httpClient: HttpClient, private logService: LogService) { }

  public getMarchePrestataires(): Observable<Marcheprestataire[]> {
    return this.httpClient.get<Marcheprestataire[]>(this.host + "/listMarcheprestataire")
      .pipe(catchError((error: any): Observable<Marcheprestataire[]> => {
        this.logService.error(error);
        return of();
      }));
  }

  public saveMarchePrestataire(url: any, marcheprestataire: Marcheprestataire): Observable<Marcheprestataire> {

    return this.httpClient.post<Marcheprestataire>(url, marcheprestataire);
  }

  public deleteMarchePrestataire(id: number):Observable<Marcheprestataire> {
    return this.httpClient.delete(`${this.host}/deleteMarcheprestataire/${id}`);
  }

  public updateMarchePrestataire(id: number, marcheprestataire:Marcheprestataire): Observable<Marcheprestataire> {

    return this.httpClient.put(`${this.host}/updateMarcheprestataire/${id}`, marcheprestataire);
  }


  public getMarchePrestataireById(id: number): Observable<Marcheprestataire> {

    return this.httpClient.get<Marcheprestataire>(`${this.host}/listMarcheprestataire/${id}`);

  }

}
