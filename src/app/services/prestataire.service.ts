import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Prestataire } from '../models/prestataire.model';
import { catchError } from 'rxjs/operators';
import { LogService } from './log.service';


@Injectable({
  providedIn: 'root'
})
export class PrestataireService {

  public host: string = "http://localhost:8080";

  constructor(private httpClient: HttpClient, private logService: LogService) { }

  public getPrestataires(): Observable<Prestataire[]> {
    return this.httpClient.get<Prestataire[]>(this.host + "/listPrestataire")
      .pipe(catchError((error: any): Observable<Prestataire[]> => {
        this.logService.error(error);
        return of();
      }));
  }

  public getPrestataireById(id: number): Observable<Prestataire> {

    return this.httpClient.get<Prestataire>(`${this.host}/listPrestataire/${id}`);

  }

  public savePrestataire(url: any, prestataire: Prestataire): Observable<Prestataire> {

    return this.httpClient.post<Prestataire>(url, prestataire);
  }


  public deletePrestataire(id: number):Observable<Prestataire> {
    return this.httpClient.delete(`${this.host}/deletePrestataire/${id}`);

  }

 

  public updatePrestataire(id: number, prestataire: Prestataire): Observable<Prestataire> {

    return this.httpClient.put(`${this.host}/updatePrestataire/${id}`, prestataire);
  }

}
