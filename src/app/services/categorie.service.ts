import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Category } from '../models/categorie.model';
import { LogService } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  public host:string="http://localhost:8080";

  constructor(private httpClient: HttpClient,private logService: LogService) { }

  public getCategories():Observable<Category[]> {
    return this.httpClient.get<Category[]>(this.host + "/listCategory")
    .pipe(catchError((error: any): Observable<Category[]> => {
      this.logService.error(error);
      return of();
    }));
 }

//  public saveCategorie(categorie:Categorie):Observable<Categorie>{

//   return this.httpClient.post<Categorie>(this.host+"/saveCategorie", categorie)
//   .pipe(catchError((error:any):Observable<Categorie> =>{
//     this.logService.error(error);
//     return of (new Categorie);
//   }));
// }



// public deleteCategorie(id: number) {
//   this.httpClient.delete(`${this.host}/deleteCategorie/${id}`).subscribe(data => {
//     console.log(data);
//   });
// }

// public updateCategorie(id: number, categorie:Categorie) {

//   this.httpClient.put(`${this.host}/updateCategorie/${id}`, categorie).subscribe(data => {
//     console.log(data);
//   });
// }
}
