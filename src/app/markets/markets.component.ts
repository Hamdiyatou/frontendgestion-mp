import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Marche } from '../models/marche.model';
import { MarcheService } from '../services/marche.service';
declare const $ :any;

@Component({
  selector: 'app-markets',
  templateUrl: './markets.component.html',
  styleUrls: ['./markets.component.css']
})
export class MarketsComponent implements OnInit, AfterViewInit {
  @ViewChild('dTable', {static:false}) dataTable:any;
  // dataTable : any;

  marches: Marche[];
  marche: Marche;
  loading: boolean = false;

  submitted?: boolean;
  marcheDialog?: boolean;
  editMode: boolean = false;


  constructor(private marcheService: MarcheService, private router: Router) { }
  ngAfterViewInit(): void {
    $(this.dataTable.nativeElement).DataTable();
  }

  ngOnInit(): void {


    // this.dataTable = $(this.table.nativeElement);
    // this.dataTable.DataTable();

    this.listMarche();

  }

  private listMarche() {
    this.marcheService.getMarches().subscribe(data => {
      if (data) {
        this.marches = data;
      } else {
        alert("Echec de chargement");
      }
    });
  }
  update(id: number) {
    this.router.navigate(['edit-market',id]);
  }

  delete(id: number) {
    let conf = confirm("Voulez-vous supprimer ?")
    if (conf) {
      this.marcheService.deleteMarche(id).subscribe(data => {
        console.log(data);
        alert("Suppression effectuée avec succès");
        this.listMarche();
      });

    } else {
      alert("Echec lors de la suppression");
    }
  }

  onAttribPrest(marketId: number) {
    // let url=marketId._links.self.href;
    this.router.navigate(['attrib-prestataire', marketId]);
  }



}
