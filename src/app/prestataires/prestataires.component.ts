import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Prestataire } from '../models/prestataire.model';
import { CategorieService } from '../services/categorie.service';
import { PrestataireService } from '../services/prestataire.service';

@Component({
  selector: 'app-prestataires',
  templateUrl: './prestataires.component.html',
  styleUrls: ['./prestataires.component.css']
})
export class PrestatairesComponent implements OnInit {



  prestataires?: Prestataire[];
  prestataire?: Prestataire;

  
  constructor( private router: Router, private prestataireService: PrestataireService) { }

  ngOnInit(): void {

    this.listPrestataire();
  }


  private listPrestataire() {
    this.prestataireService.getPrestataires().subscribe(data => {
      if (data) {
        this.prestataires = data;

      } else {
        alert("Echec de chargement");
      }
    });

  }


  delete(id: number) {
    let conf = confirm("Voulez-vous supprimer ?")
    if (conf) {
      this.prestataireService.deletePrestataire(id).subscribe(data => {
        console.log(data);
        alert("Suppression effectuée avec succès");
        this.listPrestataire();
      });

    } else {
      alert("Echec lors de la suppression");
    }
  }

  update(id: number) {
    console.log(id);
    this.router.navigate(['edit-prestataire', id]);
  }

}

