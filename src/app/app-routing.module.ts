import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { PrestatairesComponent } from './prestataires/prestataires.component';
import { MarketsComponent } from './markets/markets.component';
import { NewMarketComponent } from './new-market/new-market.component';
import { NewPrestataireComponent } from './new-prestataire/new-prestataire.component';
import { AttribPrestataireComponent } from './attrib-prestataire/attrib-prestataire.component';
import { EditMarketComponent } from './edit-market/edit-market.component';
import { EditPrestataireComponent } from './edit-prestataire/edit-prestataire.component';
import { ListMarchePrestataireComponent } from './list-marche-prestataire/list-marche-prestataire.component';
import { EditMarcheprestataireComponent } from './edit-marcheprestataire/edit-marcheprestataire.component';


const routes: Routes = [
  {
    path: "",
    redirectTo: "/login", pathMatch: "full"
  },
  {
    path: "login",
    component: LoginComponent
  },

  {
    path: "home",
    component: HomeComponent
  },

  {
    path: "prestataire",
    component: PrestatairesComponent
  },

  {
    path: "market",
    component: MarketsComponent
  },

  {
    path: "new-market",
    component: NewMarketComponent
  },

  {
    path: "new-prestataire",
    component: NewPrestataireComponent
  },
  {
    path: "attrib-prestataire",
    component: AttribPrestataireComponent
  },
  {
    path: "edit-market/:id",
    component: EditMarketComponent
  },
  {
    path: "edit-prestataire/:id",
    component: EditPrestataireComponent
  },
  {
    path: "list-marchePrestataire",
    component: ListMarchePrestataireComponent
  },
  {
    path: "edit-marcheprestataire/:id",
    component: EditMarcheprestataireComponent
  }


];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
