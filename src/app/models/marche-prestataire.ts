import { Marche } from "./marche.model";
import { Prestataire } from "./prestataire.model";

export class Marcheprestataire {
  public id?:number;
  public date_attribution?:string;
  public marche?:Marche = new Marche();
  public prestataire?:Prestataire = new Prestataire();
}



