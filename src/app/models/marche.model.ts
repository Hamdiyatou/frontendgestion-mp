export class Marche{
  public id?:number;
  public numero?:string;
  public titre?:string;
  public montant?:string;
  public description?:string;
  public dateDebutPrevu?:string;
  public dateFinPrevu?:string;
  public dateDebutEffective?:string;
  public dateFinEffective?:string;
}
