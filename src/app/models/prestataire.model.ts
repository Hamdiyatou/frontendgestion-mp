import { Category } from "./categorie.model";

export class Prestataire {
  public id?:number;
  public raison_social?:string;
  public quitus_fiscal?:string;
  public coe?:string;
  public adresse?:string;
  public tel_fixe?:string;
  public cellulaire?:string;
  public email?:string;
  public observation?:string;
  public category?:Category = new Category();

}



