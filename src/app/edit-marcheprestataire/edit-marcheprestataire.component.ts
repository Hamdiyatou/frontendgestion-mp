import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Marcheprestataire } from '../models/marche-prestataire';
import { Marche } from '../models/marche.model';
import { Prestataire } from '../models/prestataire.model';
import { MarchePrestataireService } from '../services/marche-prestataire.service';
import { MarcheService } from '../services/marche.service';
import { PrestataireService } from '../services/prestataire.service';

@Component({
  selector: 'app-edit-marcheprestataire',
  templateUrl: './edit-marcheprestataire.component.html',
  styleUrls: ['./edit-marcheprestataire.component.css']
})
export class EditMarcheprestataireComponent implements OnInit {

  id: number;
  marcheprestataire: Marcheprestataire = new Marcheprestataire();

  public prestataires: Prestataire[];
  public prestataireId: number;

  public marcheId: number;
  public marches: Marche[];

  constructor(private marketService: MarcheService, private prestataireService: PrestataireService, private route: Router, private activateRoute: ActivatedRoute, private marcheprestataireService: MarchePrestataireService) { }

  ngOnInit(): void {

    this.id = this.activateRoute.snapshot.params['id'];
    //console.log(this.id);
    this.marcheprestataireService.getMarchePrestataireById(this.id)
      .subscribe(data => {
        this.marcheprestataire = data;
        //console.log(this.marche);
      }, error => console.log(error));

    this.prestataireService.getPrestataires().subscribe(data => {
      if (data) {
        this.prestataires = data;
        // console.log(this.prestataires);
      }
    });

    this.marketService.getMarches().subscribe(data => {
      if (data) {
        this.marches = data;
        // console.log(this.marches);
      }
    });
  }

  gotoMarchePrestataireList() {
    this.route.navigate(['/list-marchePrestataire']);
  }


  onSubmit() {
    this.marcheprestataire.marche = this.marches.find((next) => next.id == this.marcheId);

    this.marcheprestataire.prestataire = this.prestataires.find((next) => next.id == this.prestataireId);

    this.marcheprestataireService.updateMarchePrestataire(this.id, this.marcheprestataire).subscribe(data => {
      alert("Modification effectuée avec succès");
      this.gotoMarchePrestataireList();
    }, error => console.log(error));
  }

}
