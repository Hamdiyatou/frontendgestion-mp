import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMarcheprestataireComponent } from './edit-marcheprestataire.component';

describe('EditMarcheprestataireComponent', () => {
  let component: EditMarcheprestataireComponent;
  let fixture: ComponentFixture<EditMarcheprestataireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMarcheprestataireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMarcheprestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
